#!/bin/bash
# tener instalado cowsay and fortune-mod

# archivos de figuras de cowsay en un arreglo
vacas=(`ls /usr/share/cows`)

# total de figuras encontradas (elementos en el arreglo)
TVACAS=${#vacas[*]}

# selecciona un numero al azar entre 0 y TVACAS
NVACA=$((RANDOM%$TVACAS))

# nombre del archivo cowsay a utilizar
vaca=${vacas[$NVACA]}

# forma el saludo con fortune y cowsay
fortune | cowsay -f $vaca
