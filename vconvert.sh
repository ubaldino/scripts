#!/bin/sh
#Conversion de flv(s) a .3gp

if [ $# -eq 0 ]; then
    echo "Uso: $0 [lista de ficheros flv]"
    exit
fi

if [ ( = * ]; then
    lista=`ls`
else
    lista=$*
fi

for fichero in $lista; do
    echo "Procesando $fichero."
ficherosalida=`echo $fichero|cut -f1 -d"."`.3gp
ffmpeg -i $fichero -s qcif -vcodec libx264 -r 25 -b 200 -acodec libfaac -ab 64 -ac 1 -ar 8000 $ficherosalida
done

echo "Proceso concluido.")
