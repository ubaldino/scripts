#!/usr/bin/python
#-*- coding: utf-8 -*-
__author__ = 'ubaldino Zurita'
import sys, urllib2, re, urllib
def barra(block_count, block_size, total_size):
  total_kb = total_size / 1024
  descargado = block_count * (block_size / 1024)
  porcentaje = descargado * 100 / total_kb
  texto_barra =  "=> " + str(descargado) + " kb de " + str(total_kb / 1024.0) + " Mb (" + str(porcentaje) + "%)"
  sys.stdout.write("\b" * (len(texto_barra)))
  sys.stdout.write(texto_barra)
  sys.stdout.flush()

if len(sys.argv) > 1:
  titulo = sys.argv[1]
  id = 0
  while id == 0:
    if titulo == '*':  url = "http://babel.scesi.memi.umss.edu.bo/search?q=*"
    else:  url = "http://babel.scesi.memi.umss.edu.bo/search?q=%s*"%(titulo)
    try: html = urllib2.urlopen(url).read()
    except: print "\033[1;31m **** Error de connexión ****\033[00m";id=1
    else:
      titulos = re.findall("class=\"details\">\W*<h1>([\w \d\.\:\-\'á-úÁ-Ú].*)</h1>", html)
      links = re.findall("books\/\w{1,}", html);  count = 0
      for titulo in titulos: count=count+1; print " \033[1;33m%d\t\033[1;37m%s\033[00m"%(count, titulo)
      if len(titulos) == 0: print "\033[1;31m **** No se hallaron coincidencias ****\033[00m";id=1
      else:
        id = input("\033[1;31mIngrese el libro a descargar o [CERO] para buscar otro libro:\033[00m ")
        if id == 0:
          titulo = raw_input("\033[1;33mIngrese el libro a buscar:\033[00m ")
          if titulo[0] == '\\': titulo = '*'
        else:
          url_des = "http://babel.scesi.memi.umss.edu.bo/%s/download"%(links[id-1])
          print "\033[1;36m[descargando]\033[1;37m %s.zip"%(titulos[id-1])
          try: urllib.urlretrieve(url_des, "%s.zip"%(titulos[id-1]), reporthook=barra)
          except: print "\033[1;31m **** Error de descarga ****\033[00m"
          else: print "\t\033[1;31mdescarga finalizada\033[00m"
          id=1

else: 
  print """
\033[1;37m Ejemplo para el uso del script:

------------------------------
\033[1;32m./babel.py <titulo>
./babel.py <iniciales>
./babel.py \* <= lista todos los libros
\033[1;37m------------------------------

Talvez algun dia, se convierta
en cliente babel en:
OBI-GNU/Linux
FOS-GNU/Linux\033[00m
"""
