#!/bin/bash
# Este script lo realize para alivianizar
# fotos para subirlos a 
# galeria.scesi.memi.umss.edu.bo
#*********************
# escanea todas las fotos de una carpeta 
# para luego reducirles el tamanio
rename 'y/A-Z/a-z/' * ; 
mkdir reduccion; 
for i in `ls *.jpg`; 
do convert -verbose -quality 80 -strip $i ./reduccion/$i;
done;
echo FIN;
