#!/bin/bash

# dentro de un ciclo sin condiciones se ejecuta el script
while :                                                                                  
do
  # se formatea la hora y la fecha como la veremos en pantalla
  cmd=`date +"%H:%M:%S %F"`
  # se guarda la posición actual del cursor
  echo -n -e "\033[s"

  # estas dos líneas puedes descomentarlas y su resultado es que la línea superior de la consola
  # siempre estará limpia, solo el reloj, pero causa parpadeo
  #tput cup 0 0
  #tput el

  # se calcula el total de columnas de la terminal o consola y se posiciona el cursor en esa
  # posición menos 19 caracteres que es lo que mide 'HH:MM:SS AAAA-MM-DD'
  C=$((`tput cols` - 19))
  tput cup 0 $C

  # establecemos el color del reloj en verde invertido
  # setaf 2 = verde, smso = invertido
  COLOR=`tput setaf 2; tput smso`

  # se regresa a texto normal, sino todo saldría verde
  NORMAL=`tput sgr0`
                      
  # mandamos a la pantalla la variable con la fecha y hora junto con los cambios de color
  echo -n $COLOR$cmd$NORMAL
                        
  # se reestablece el cursor a donde se encontraba originalmente
  echo -n -e "\033[u"
                          
  # el ciclo se ejecuta cada segundo
  sleep 1
done
